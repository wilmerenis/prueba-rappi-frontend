# Levantamiento del Proyecto

dentro de la carpeta raiz del proyecto realizar los siguientes comandos:

```sh

$ npm install

$ npm install json-server

$ json-server --watch db.json --port 3001

$ npm start

```

Herramientas utilizadas en esta prueba:


| Herramientas |      Repositorio |
|----------|:-------------:|
| MaterialUI  |  [https://material-ui.com/]|
| StyledComponents  |    [http://react-animations.herokuapp.com/]|
| React Animations | [https://reactstrap.github.io/]|