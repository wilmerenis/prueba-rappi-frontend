import './App.css';
import React, { useState, useEffect } from 'react';
import Card from './components/Card';
import Cart from './components/Cart';
import Navbar from './components/Navbar'; 
import { zoomIn } from 'react-animations';
import Sidebar from './components/Sidebar';
import Spinner from './components/Spinner';
import ShopContext from './context/ShopContext';
import { Container, Col, Row } from 'reactstrap';
import styled, { keyframes } from 'styled-components';
import ErrorBoundary from './components/ErrorBoundary';

const initialState = {
  cart: [],
  query: null,
  products: [],
  fetched: false,
  fetching: true,
  hasError: false,
  openCart: false,
}

const bounceAnimation = keyframes`${zoomIn}`;

const BounceIn = styled.div`animation: ${props => ( props.time * 0.15 )}s ${bounceAnimation};`;

function App() {
  const [state, setState] = useState(initialState)
  const [cart, setCart] = useState([])
  const { products, fetched, fetching, query, hasError } = state

  useEffect(() => {
    const verifyStorage = async () => {
      const ShopCart = await JSON.parse(localStorage.getItem('ShopCart'))
      if(ShopCart) setCart(ShopCart)
    }

    verifyStorage();
  }, [])
  
	useEffect(() => {
		const fetchData = async () => {
      let path = query ? `http://localhost:3001/products?sublevel_id=${query}` : 'http://localhost:3001/products'
      console.log(state, 'aqui')
      setState({ ...state, fetching: true })
			const result = await fetch(path)
        .then((result) => result.json())
        .then((data) => setState({ ...state, products: data, fetched: true, fetching: false }))
        .catch((err) => setState({ ...state, fetching: false, hasError: true }))
    };
    fetchData();
  }, [query])
  
  const handleQuery = (query) => setState({...state, query})

  const addProductToCart = product => {
    let updatedCart = [...cart];
    let updatedItemIndex = updatedCart.findIndex(
      item => item.id === product.id
    );

    if (updatedItemIndex < 0) {
      updatedCart.push({ ...product, quantity: 1 });
    } else {
      let updatedItem = { ...updatedCart[updatedItemIndex] };
      updatedItem.quantity++;
      updatedCart[updatedItemIndex] = updatedItem;
    }
    setCart(updatedCart);
    localStorage.setItem('ShopCart', JSON.stringify(updatedCart) )
  };

  const removeProductFromCart = productId => {
    let updatedCart = [...cart];
    let updatedItemIndex = updatedCart.findIndex(
      item => item.id === productId
    );

    let updatedItem = {
      ...updatedCart[updatedItemIndex]
    };
    updatedItem.quantity--;
    if (updatedItem.quantity <= 0) {
      updatedCart.splice(updatedItemIndex, 1);
    } else {
      updatedCart[updatedItemIndex] = updatedItem;
    }
    setCart(updatedCart);
    localStorage.setItem('ShopCart', JSON.stringify(updatedCart) )
  };

  const handleOpenCart = () => setState({ ...state, openCart: !state.openCart })
  console.log(state, 'aqui3')
  
  return (
    <ShopContext.Provider
      value={{ 
        cart: cart,
        openCart: state.openCart,
        products: state.products, 
        addToCart: addProductToCart, 
        handleOpenCart: handleOpenCart,
        removeFromCart: removeProductFromCart
      }}
    >
      <Navbar 
        cartItemNumber={cart.reduce((count, curItem) => {
          return count + curItem.quantity
        }, 0)}

        handleOpenCart={handleOpenCart}
      />
      <Row noGutters style={{ flex: 1 }} >
        <Col md={3}>
          <Sidebar handleQuery={handleQuery}/>
        </Col>
        <Col md={9} style={{ position: 'static' }} >
          <Container style={{ paddingBottom: 40 }} >
            <Row cellSpacing={8} >
              { fetched ? products.map( (data, i) => (
                <Col md={3} key={i} >
                  <BounceIn time={i}>
                    <Card data={data} addToCart={addProductToCart} />
                  </BounceIn>
                </Col>
              ) ) : fetching ? <Spinner/> : hasError && <ErrorBoundary/>}
            </Row>
          </Container>
        </Col>
      </Row>
      <Cart />
    </ShopContext.Provider>
  );
}

export default App;
