import styled from 'styled-components'

const Empty = styled.div`
	width: 100px;
	height: 100px;
	margin: 0 auto;
	opacity: .3;
	background-size: cover;
	background-position: center center;
	background-image: url(${require('./box.svg')});
`

export { Empty }