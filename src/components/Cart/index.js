import React from 'react';
import { Empty } from './index.style';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ShopContext from '../../context/ShopContext';
import { ShoppingBasket, Delete } from '@material-ui/icons';
import { 
	Hidden,
	DialogTitle, DialogActions, DialogContent, 
	withStyles, List, ListItem, ListItemAvatar, 
	ListItemText, Avatar, ListItemSecondaryAction, IconButton
} from '@material-ui/core';

const styles = theme => ({
  root: {
		width: '100%',
    backgroundColor: theme.palette.background.paper,
	},
	dialogContent: {
		padding: '0 0 12px'
	},
	dialogTitle: { padding: '12px 12px 6px' },
	button: {
    background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 48,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
  }
});

function ShopCart({classes}) {
	return (
		<ShopContext.Consumer>
			{context => (
				<Dialog
					fullWidth={true}
					open={context.openCart}
					onClose={context.handleOpenCart}
					scroll='paper'
					aria-labelledby="scroll-dialog-title"
					maxWidth="sm"
				>
					<DialogTitle classes={{ root: classes.dialogTitle }} id="scroll-dialog-title" >
						<p style={{ display: 'flex', justifyContent: 'space-between' }}>
							<span>
								Carrito
							</span>
							<span style={{ color: '#adadad', fontSize: '1rem' }} >
								{`Total: ${context.cart.reduce((count, product) => {
									return parseFloat( ( count + ( product.quantity * parseFloat( product.price.slice(1).replace(/,/g, ".") ) ) ).toFixed(3) )
								}, 0)}$`}
							</span>
						</p>
					</DialogTitle>
					<DialogContent classes={{ root: classes.dialogContent }} >
						<div className={classes.root}>
							<List dense={false}>
								{context.cart.length ? context.cart.map( (data, i) => (
									<ListItem key={i} >
										<Hidden only="xs">
											<ListItemAvatar>
												<Avatar>
													<ShoppingBasket />
												</Avatar>
											</ListItemAvatar>
										</Hidden>
										<ListItemText	primary={`${data.name}`}/>
										<ListItemText secondary={`x ${data.quantity} `}/>
										<ListItemText secondary={`${(parseFloat(data.price.slice(1).replace(/,/g, ".")) * data.quantity).toFixed(3)} $`}/>
										<ListItemSecondaryAction onClick={() => context.removeFromCart(data.id)}>
											<IconButton aria-label="Delete">
												<Delete />
											</IconButton>
										</ListItemSecondaryAction>
									</ListItem>
								) ): <Empty/>}
							</List>
						</div>
					</DialogContent>
					<DialogActions >
						<Button onClick={context.handleOpenCart} variant="outlined" className={classes.button}>
							Cerrar
						</Button>
					</DialogActions>
				</Dialog>
			)}
		</ShopContext.Consumer>
	);
}

export default withStyles(styles)(ShopCart);