import styled from 'styled-components'

const Styles = styled.article`
	margin-top      : 10px;
	width           : 100%;
	height: auto;
	box-shadow      : 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
	border-radius   : 4px;
	background-color: #fff;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	p { margin: 0; }
	.Card__header { text-transform: uppercase; }

	.Card__title {
		color         : #adadad;
		overflow      : hidden;
		font-size     : .85rem;
		font-weight   : 600;
		white-space   : nowrap;
		text-overflow : ellipsis;
		padding       : 10px 10px 0 10px;
	}

	.Card__price {
		color: #444;
		font-size: 1rem;
		font-weight: 500;
	}

	.Card__info {
		font-size: .8rem;
		font-weight: 500;
	}

	.Card__content {
		overflow: hidden;
		display: flex;
		align-items: center;
		padding    : 10px;

	}

	.Card__footer {
		border-top: 1px solid rgba(0,0,0,.15);
		overflow: hidden;
		text-align: right;
		font-size: .8rem;
		font-weight: 600;
		padding: 5px 10px;
		display: flex;
		justify-content: space-between;
		.Card__content__icon {
			height: 30px;
			width: 30px;
			background-image: url(${require('./assets/new.svg')});
			background-size: cover;
		}
	}
`

export {
	Styles
}