import React from 'react';
import { Styles } from './index.style';
import { Button, withStyles } from '@material-ui/core';

const styles = theme => ({
	fontSize: '.5rem',
	fontWeight: 600,
	extendedIcon: { marginRight: theme.spacing.unit },
	button: {
    background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 38,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
  }
});

function Card({data, classes, addToCart}) {
	return(
		<Styles>
			<div className="Card__header">
				<div className="Card__title">{data.name}</div>
			</div>
			<div className="Card__content">
				<div className="Card__content__info" >
					<p className="Card__price">{data.price}</p>
					<p className="Card__info">{data.shortDescription}</p>
				</div>
			</div>
			<div className="Card__footer">
				<div className="Card__content__icon"></div>
				<Button size="small" className={classes.button} onClick={() => addToCart(data)} >Add</Button>
			</div>
		</Styles>

	)
}

export default withStyles(styles)(Card)