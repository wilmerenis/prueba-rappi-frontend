import PropTypes from 'prop-types';
import ErrorBoundary from '../ErrorBoundary';
import React, { useState, useEffect } from 'react';
import { 
	withStyles,
	ListSubheader, List, ListItem, 
	ListItemIcon, ListItemText, Collapse, MenuItem, Hidden
} from '@material-ui/core';
import { ExpandMore, ExpandLess, Send, KeyboardArrowRight as Keyboard } from '@material-ui/icons'; 

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 320,
    backgroundColor: theme.palette.background.paper,
	},
  nested: { paddingLeft: theme.spacing.unit * 4 },
});

function SubHead({ categories, classes, handleQuery, nested}) {
	const [state, setState] = useState({})

	const handleClick = ( item ) => {
    setState( prevState => ( 
      { [ item ]: !prevState[ item ] } 
    ) )
  }

	return (
		<>
			{categories.map((data, i) => {
				if(data.sublevels) {
					return(
						<React.Fragment key={i}>
							<MenuItem 
								button 
								onClick={() => handleClick( `${data.name}.${i}` )} 
								className={nested && classes.nested} >
								<ListItemIcon>
									<Keyboard />
								</ListItemIcon>
								<ListItemText inset primary={`${data.name}`} />
								{state[`${data.name}.${i}`] ? <ExpandLess /> : <ExpandMore />}
							</MenuItem>
							<Collapse in={state[`${data.name}.${i}`]} timeout="auto" unmountOnExit>
								<List component="div" disablePadding className={nested && classes.nested}>
									<SubHead {...Object.assign({}, {categories: data.sublevels, classes, handleQuery})} nested={true} />
								</List>
							</Collapse>
						</React.Fragment>
					)
				}

				return(
					<ListItem button key={i} className={nested && classes.nested} onClick={() => handleQuery(data.id)} >
						{/* <ListItemIcon>
							<Send />
						</ListItemIcon> */}
						<ListItemText primary={data.name} />
					</ListItem>
				)
			})}
		</>
	)
}

function Menu({classes, handleQuery}) {
	const [categories, setCategories] = useState([])
	const [hasError, setError] = useState(false)

	useEffect(() => {
		const fetchData = async () => {
			const result = await fetch('http://localhost:3001/categories')
				.then((result) => result.json())
				.then((data) => setCategories(data))
				.catch((err) => setError(true))
    };
    fetchData();
	}, [])

	return(
		<>
			{!hasError ? 
				<List component="nav" subheader={<ListSubheader component="div">Categories</ListSubheader>} className={classes.root}>
					<SubHead {...Object.assign({}, {categories, classes, handleQuery})} />
				</List> : <Hidden only='xs'><ErrorBoundary/></Hidden>
			}
		</>
	)
}

Menu.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Menu);