import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Menu as MenuIcon, ShoppingCart } from '@material-ui/icons';
import { IconButton, AppBar, Typography, Toolbar, Badge } from '@material-ui/core';

const styles = theme => ({
  grow: { flexGrow: 1 },
  menuButton: { marginLeft: -12, marginRight: 20 },
  badge: {
    top: '50%',
    right: -3,
    // The border color match the background color.
    backgroundColor: '#2196f3',
    border: `2px solid ${ theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900] }`,
  },
});

function Navbar({ classes, cartItemNumber, handleOpenCart }) {
	return(
		<div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            Products
          </Typography>
          <IconButton onClick={handleOpenCart} className={classes.menuButton} color="inherit" aria-label="Menu">
          <Badge badgeContent={cartItemNumber} color="primary" classes={{ badge: classes.badge }}>
            <ShoppingCart />
          </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
	)
}

/// PropsTypes ///
Navbar.prototype = { classes: PropTypes.object.isRequired }
/// PropsTypes ///

export default withStyles(styles)(Navbar)
