import styled from 'styled-components'

const Styles = styled.div`
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%,-50%);
	width: 200px;
	padding: 15px 20px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	h1 {
		text-align: center;
		font-size: 1rem;
		font-weight: 500;
	}
`

export { Styles }