import React from 'react'
import { Styles } from './index.style'
import { Button, withStyles} from '@material-ui/core'

const styles = {
  button: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 38,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px var(--box-shadow)',
  },
};

class ErrorBoundary extends React.Component {
	render() {
		const { classes } = this.props
		return (
			<Styles>
				<div className="ErrorBoundary__image" >
					<img src={require('./close.svg')} className="img-fluid" alt="close" />
				</div>
				<h1>Ups... Something went wrong.</h1>
				<Button 
					className={classes.button}
					variant="outlined" 
					onClick={() => window.location.reload() } 
				>
					Recargar
				</Button>
			</Styles>
		);
	}
}

export default withStyles(styles)(ErrorBoundary)